<?php

namespace Drupal\custom_admin_theme\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for the custom administration theme.
 */
class CustomAdminThemeSettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_admin_theme_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['custom_theme'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Theme'),
      '#default_value' => \Drupal::state()->get('custom_admin_theme.custom_theme', ''),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValue('custom_theme');

    // Check if the text field is empty.
    if (empty($value)) {
      // Delete the value of the variable.
      \Drupal::state()->delete('custom_admin_theme.custom_theme');
      $this->messenger()->addMessage($this->t('The custom theme value has been removed.'));
    }
    else {
      // Store the value in the variable.
      \Drupal::state()->set('custom_admin_theme.custom_theme', $value);
      $this->messenger()->addMessage($this->t('The information has been saved correctly.'));
    }

    \Drupal::service('library.discovery')->clearCachedDefinitions();
  }

}
