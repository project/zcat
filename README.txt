CONTENTS
--------

Introduction
Requirements
Installation
Configuration
Maintainers


INTRODUCTION
------------
Custom admin theme is a module for the frontend development of the administration theme.

In many occasions it is necessary to fix some styles found in the administration themes
and it is not convenient to use our "custom-theme", so it is necessary to use the module.

Module project page:
http://drupal.org/project/zcat

To submit bug reports and feature suggestions, or to track changes:
http://drupal.org/project/issues/zcat

REQUIREMENTS
------------
None.

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. 
Visit: https://www.drupal.org/docs/extending-drupal/installing-modules

CONFIGURATION
-------------
The module allows access to change styles in the administration in two ways, through the included css or
through a file placed in /theme/custom/[yourCustomTheme]/css/custom-admin.css.

To let the module know the name of your theme you can indicate it in the configuration screen: 
"/admin/config/custom-admin-theme, type the name of your theme and click on save."

MAINTAINER
-----------
gserramar - https://www.drupal.org/user/3673360